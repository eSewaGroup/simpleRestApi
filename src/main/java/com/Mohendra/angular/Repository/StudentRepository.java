package com.Mohendra.angular.Repository;

import com.Mohendra.angular.Dto.StudentCountDto;
import com.Mohendra.angular.Modal.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Map;

@Repository
public interface StudentRepository extends JpaRepository<Student, String> {
    public Student findByStudentId(String studentID);

    @Query("SELECT new com.Mohendra.angular.Dto.StudentCountDto(count(case when s.status=0 then s.id end) as inactive,"+
            "count (case when s.status =1 then s.id end)as active,"+
    "count(s) as total) from Student s ")
    public StudentCountDto getStudentCount();
}
