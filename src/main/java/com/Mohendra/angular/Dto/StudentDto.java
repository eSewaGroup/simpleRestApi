package com.Mohendra.angular.Dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StudentDto {
    private String fullName;
    private String studentId;
    private int phoneNumber;
    private String level;
}
