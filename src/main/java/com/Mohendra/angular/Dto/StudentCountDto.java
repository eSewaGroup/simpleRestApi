package com.Mohendra.angular.Dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class StudentCountDto {
    private long inactive;
    private long active;
    private long total;
}
