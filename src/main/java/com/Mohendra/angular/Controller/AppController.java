package com.Mohendra.angular.Controller;


import com.Mohendra.angular.Dto.StudentCountDto;
import com.Mohendra.angular.Dto.StudentDto;
import com.Mohendra.angular.Modal.Student;
import com.Mohendra.angular.Service.Interface.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class AppController {
    @Autowired
    private StudentService studentService;

    @RequestMapping("/student-count")
    public StudentCountDto countStudents(){ return studentService.getStudentCount();}

    @RequestMapping("/all-students")
    public List<Student> findAllStudents(){
        return studentService.findAll();
    }

    @RequestMapping("/student/{studentId}")
    public Student findIndividual(@PathVariable("studentId") String studentId) throws Exception{
        return studentService.findByStudentId(studentId);
    }

    @RequestMapping(value = "/update/{studentId}", method = RequestMethod.PUT)
    public Student updateStudent(@PathVariable String studentId, @RequestBody StudentDto studentDto) throws Exception {
        return studentService.update(studentId, studentDto);
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Student addStudent(@RequestBody StudentDto studentDto){
       return studentService.insert(studentDto);
    }
    @RequestMapping(value = "/delete/{studentId}", method = RequestMethod.PUT)
    public void deleteStudent(@PathVariable String studentId) throws Exception {
        studentService.terminateStudent(studentId);
    }

}
