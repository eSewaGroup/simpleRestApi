package com.Mohendra.angular.Service.Implementation;

import com.Mohendra.angular.Dto.StudentCountDto;
import com.Mohendra.angular.Dto.StudentDto;
import com.Mohendra.angular.Exception.ResourceNotFoundException;
import com.Mohendra.angular.Modal.Status;
import com.Mohendra.angular.Modal.Student;
import com.Mohendra.angular.Repository.StudentRepository;
import com.Mohendra.angular.Service.Interface.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Transactional
@Service
public class StudentServiceImpl implements StudentService {
    @Autowired
    private StudentRepository studentRepository;
    @Override
    public List<Student> findAll() {
        return studentRepository.findAll();
    }

    @Override
    public Optional<Student> findById(String studentId) throws Exception {
        return studentRepository.findById(studentId);
    }

    @Override
    public Student insert(StudentDto studentDto) {
        Student student= new Student();
        student.setFullName(studentDto.getFullName());
        student.setPhoneNumber(studentDto.getPhoneNumber());
        student.setLevel(studentDto.getLevel());
        student.setStudentId(studentDto.getStudentId());
        student.setStatus(Status.ACTIVE);
        return studentRepository.save(student);
    }


    @Override
    public Student update(String studentId, StudentDto studentDto) throws Exception {
        Student student = new Student();

        try{
            student=studentRepository.findByStudentId(studentId);
            student.setFullName(studentDto.getFullName());
            student.setLevel(studentDto.getLevel());
            student.setPhoneNumber(studentDto.getPhoneNumber());
            return studentRepository.save(student);
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Student deleteById(String studentId) throws Exception {
        return null;
    }

    @Override
    public StudentCountDto getStudentCount() {
        return studentRepository.getStudentCount();
    }


    @Override
    public Student terminateStudent(String studentId) {
        try{
            Student student = studentRepository.findByStudentId(studentId);
            student.setStatus(Status.INACTIVE);
            return studentRepository.save(student);
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Student findByStudentId(String studentId) {
        return studentRepository.findByStudentId(studentId);
    }
}
