package com.Mohendra.angular.Service.Interface;

import com.Mohendra.angular.Dto.StudentCountDto;
import com.Mohendra.angular.Dto.StudentDto;
import com.Mohendra.angular.Modal.Student;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface StudentService {
    public Student terminateStudent(String studentId);
    public Student findByStudentId(String studentId);
    List<Student> findAll();
    Optional<Student> findById(String studentId) throws Exception;
    Student insert(StudentDto entity);
    Student update(String identity, StudentDto entity) throws Exception;
    Student deleteById(String studentId) throws Exception;
    StudentCountDto getStudentCount();
}
